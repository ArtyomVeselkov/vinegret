<?xml version="1.0" encoding="utf-8"?>


<!--
	- Режимы сохранения:
	  - в корень папки
	  - страницы в отдельную папку
	  ++ все абсолютные пути
-->

<xsl:stylesheet
        version="2.0"
		xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
		xmlns:xhtml="http://www.w3.org/1999/xhtml">
    <!--    
<xsl:output name="std-out-format" method="xhtml" indent="yes"
            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"
            doctype-public="-//W3C//DTD XHTML 1.0 Strict//EN"/><xsl:output name="section-format" method="xhtml" indent="no"
            doctype-system="http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd"
            doctype-public="-//W3C//DTD XHTML 1.0 Transitional//EN"/>        
         
	 -->
	 
	 
<!-- Создание переменных с содержимым шапки (стили, скрипты, ...) -->
<!-- Заголовок -->

<!-- Параметр командной строки .... -langVer="ukr" или .... -langVer="rus" -->
<xsl:param name="langVer" select="langVer"/>
<xsl:param name="emacsVer" select="emacsVer"/>

<xsl:variable name="CMN_USE_CONF">
    <xsl:choose>
        <xsl:when test="$langVer = '2-ukr'">2-ukr</xsl:when>
        <xsl:when test="$langVer = '1-rus'">1-rus</xsl:when>
        <xsl:otherwise>1-rus</xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="CMN_USE_EMACS">
    <xsl:choose>
        <xsl:when test="$emacsVer = '24.4'">24.4</xsl:when>
        <xsl:when test="$emacsVer = '24.5'">24.5</xsl:when>
        <xsl:otherwise>24.4</xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="CMN_TITLE_TAG">
	<xsl:value-of select="/*/xhtml:head/xhtml:title"/>
</xsl:variable>
<!-- Стили -->
<xsl:variable name="CMN_HEAD_STYLES_TAGS">
	<xsl:copy-of select="/*/xhtml:head//xhtml:style"/>
</xsl:variable>
<!-- Скрипты -->
<xsl:variable name="CMN_HEAD_SCRIPTS_TAGS">
	<xsl:copy-of select="/*/xhtml:head//xhtml:script"/>
</xsl:variable>
<!-- Линки -->
<xsl:variable name="CMN_LINKS_TAGS">
	<xsl:copy-of select="/*/xhtml:head//xhtml:link"/>
</xsl:variable>
<!-- Все мета-теги, кроме описания и ключевых слов -->
<xsl:variable name="CMN_META_TAGS">
	<xsl:copy-of select="/*/xhtml:head//xhtml:meta[@name != 'description'][@name != 'keywords']"/>
</xsl:variable>
<!-- Мета-тег описания -->
<xsl:variable name="CMN_META_DESC_TAG">
	<xsl:copy-of select="/*/xhtml:head//xhtml:meta[@name = 'description']"/>
</xsl:variable>
<!-- Мета-тег ключевых слов -->
<xsl:variable name="CMN_META_KEYW_TAG">
	<xsl:copy-of select="/*/xhtml:head//xhtml:meta[@name = 'keywords']"/>
</xsl:variable>
<!-- Все теги между содержанием и первым слайдом -->
<xsl:variable name="CMN_BODY_FIRST_TAGS">
    <xsl:choose>
        <xsl:when test="$CMN_USE_EMACS = '24.5'"><xsl:copy-of select="/*/xhtml:body//xhtml:div[@id='table-of-contents']/following-sibling::node()[count(.|/*/xhtml:body//xhtml:div[starts-with(@id, 'outline-container-orgheadline1')]/preceding-sibling::node()) = count(/*/xhtml:body//xhtml:div[starts-with(@id, 'outline-container-orgheadline1')]/preceding-sibling::node())]"/></xsl:when>
        <xsl:otherwise><xsl:copy-of select="/*/xhtml:body//xhtml:div[@id='table-of-contents']/following-sibling::node()[count(.|/*/xhtml:body//xhtml:div[starts-with(@id, 'outline-container-sec-1')]/preceding-sibling::node()) = count(/*/xhtml:body//xhtml:div[starts-with(@id, 'outline-container-sec-1')]/preceding-sibling::node())]"/></xsl:otherwise>
    </xsl:choose>
</xsl:variable>
<!-- Количество слайдов -->
<xsl:variable name="CMN_TOTAL_PAGES_COUNT">
    <xsl:choose>
        <xsl:when test="$CMN_USE_EMACS = '24.5'"><xsl:value-of select="count(/*/xhtml:body//xhtml:div[starts-with(@id, 'outline-container-orgheadline')])" /></xsl:when>
        <xsl:otherwise><xsl:value-of select="count(/*/xhtml:body//xhtml:div[starts-with(@id, 'outline-container-sec')])" /></xsl:otherwise>
    </xsl:choose>
</xsl:variable>
<!-- Пользовательские дополнительные стили, скрипты, ... -->
<xsl:variable name="CMN_USER_HEAD_TAG">
	<style  type="text/css">
		h2 { 
			display: block; 
		}
		#table-of-contents {
			display: block !important;
            visibility: visible !important;
			z-index: 100;
			top: 5em;
            padding-right: 5px;
		}
		table#header {
			margin-top: 5px;
		}
		#header-table-prev-page-link a {
			color: rgb(255, 102, 102);
			border-radius: 5px;
			border: #F66 1px solid;
			padding: 3px;
		}
		#header-table-next-page-link a {
			color: rgb(255, 102, 102);
			border-radius: 5px;
			border: #F66 1px solid;
			padding: 3px;
		}
		#header-table-toc-link a {
			color: rgb(255, 180, 180);
		}
		div.outline-2 { 
			margin-right: 40px; 
		}
		.section-number-2:after {
			content: ".";
		}
	</style>
</xsl:variable>
   
   
<!-- Первый элемент в титультном TOC -->    
<xsl:variable name="CMN_USER_TITLE_TOC_UL_FIRST_ELEMENT">
    <!-- A la sidebar in title TOC -->
    <img style="max-width: 55%; border-radius: 20px; box-shadow: 0px 0px 30px gray;" src="http://www.googledrive.com/host/0B_3bS9-iGMiQZVlhdFIzR25ZT1U/src/mf/.images/.pack/.theme/flame/heart_panda_flames-cut.jpg" hspace="5" align="right" vspace="5" />
</xsl:variable>

<!-- Элементы управления -->
<xsl:variable name="CMN_USER_HEADER_READ_ALT_LANG_CAPTION">
    <xsl:choose>
        <xsl:when test="$CMN_USE_CONF = '2-ukr'">Версия страницы на русском языке</xsl:when>
        <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="CMN_USER_HEADER_READ_ALT_LANG_DOMAIN">
    <xsl:choose>
        <xsl:when test="$CMN_USE_CONF = '2-ukr'">http://biochem-love.appspot.com/</xsl:when>
        <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
</xsl:variable>


<xsl:variable name="CMN_USER_HEADER_READ_ALT_LANG_RELPATH">
    <xsl:choose>
        <xsl:when test="$CMN_USE_CONF = '2-ukr'">part1/</xsl:when>
        <xsl:otherwise></xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="CMN_USER_HEADER_NEXT_TEXT">
    <!-- Набор настроек в зависимости от профиля компиляции -->
    <xsl:choose>
        <xsl:when test="$CMN_USE_CONF = '2-ukr'">Вперед</xsl:when>
        <xsl:otherwise>Вперёд</xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="CMN_USER_HEADER_PREV_TEXT">
    <!-- Набор настроек в зависимости от профиля компиляции -->
    <xsl:choose>
        <xsl:when test="$CMN_USE_CONF = '2-ukr'">Назад</xsl:when>
        <xsl:otherwise>Назад</xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="CMN_USER_HEADER_TOC_TEXT">
    <!-- Набор настроек в зависимости от профиля компиляции -->
    <xsl:choose>
        <xsl:when test="$CMN_USE_CONF = '2-ukr'">Зміст</xsl:when>
        <xsl:otherwise>Содержание</xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<!-- Пользовательская шапка -->
<xsl:variable name="CMN_USER_HEADER_TEXT">
    <!-- Набор настроек в зависимости от профиля компиляции -->
    <xsl:choose>
        <xsl:when test="$CMN_USE_CONF = '2-ukr'">
            <a href="http://biochem-love.appspot.com/part2/index.html" target="_blank">
            <!-- Продолжение сайта: &#171;Семь бед &#8211; один ответ или самопознание продолжается&#187; -->
            Продовження сайту: &#8220;<b>ПО</b>беда&#8221; капитана Врунгеля или ПрограммноеОбеспечение &#8220;Беда&#8221;
            </a>
        </xsl:when>
        <xsl:otherwise>
            <a href="http://biochem-love.appspot.com/part2/index.html" target="_blank">
            <!-- Продолжение сайта: &#171;Семь бед &#8211; один ответ или самопознание продолжается&#187; -->
            Продолжение сайта: &#8220;<b>ПО</b>беда&#8221; капитана Врунгеля или ПрограммноеОбеспечение &#8220;Беда&#8221;
            </a>
        </xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="CMN_USER_TITLEPAGE_INTRO">
    <!-- Набор настроек в зависимости от профиля компиляции -->
    <xsl:choose>
        <xsl:when test="$CMN_USE_CONF = '2-ukr'">
            <h1 class="title">Біохімія кохання
            <br /> <i>Чому й навіщо ми кохаємо?</i>
            <br /> <i style="color: #7B8BA2; font-size: 90%; text-shadow: 0px 1px 0px white;">... і чи треба від цього лікуватися</i></h1>

            <div style="color:#26BD63;position:absolute;top:4em;width:300px;">
                Для перегляду мозог не надається, приходити<span style="white-space:pre;"> з власним.</span>
            </div>
        </xsl:when>
        <xsl:otherwise>
            <h1 class="title">Биохимия любви 
                <br /> <i>Почему и зачем мы любим?</i>
                <br /> <i style="color: #7B8BA2; font-size: 90%; text-shadow: 0px 1px 0px white;">... и надо ли от этого лечиться?</i>
            </h1>
            
            <div style="color:#26BD63;position:absolute;top:4em;width:300px;">
                Для просмотра мозги не прилагаются, приходить <span style="white-space:pre;"> со своими.</span>
            </div>
        </xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<xsl:variable name="CMN_USER_TITLEPAGE_FOOTER">
    <!-- Набор настроек в зависимости от профиля компиляции -->
    <xsl:choose>
        <xsl:when test="$CMN_USE_CONF = '2-ukr'">
        </xsl:when>
        <xsl:otherwise>
            <!--<center><a rel="author" href="https://plus.google.com/116589555418770019015"><img src="https://ssl.gstatic.com/images/icons/gplus-32.png" alt="Est.Sensus G+1" width="31" height="31" /></a></center>-->
        </xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<!-- Пользовательский футер -->
<xsl:variable name="CMN_USER_FOOTER">
    <!-- Набор настроек в зависимости от профиля компиляции -->
    <xsl:choose>
        <xsl:when test="$CMN_USE_CONF = '2-ukr'">
            <img src="http://biochem-love.pp.ua/dta?profile=globmetrics&amp;log=c&amp;incer=u&amp;redir=9veritasScipt" style="float: right;" width="300px" />
            
            <!-- Yandex.Metrika counter --><script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter32784965 = new Ya.Metrika({ id:32784965, clickmap:true, trackLinks:true, accurateTrackBounce:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><!-- /Yandex.Metrika counter -->

            <script>
              (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
              (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
              m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
              })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

              ga('create', 'UA-62017764-2', 'auto');
              ga('send', 'pageview');
            </script>
            
        </xsl:when>
        <xsl:otherwise>
            <img src="http://biochem-love.appspot.com/dta?profile=globmetrics&amp;log=c&amp;incer=u&amp;redir=9veritasScipt" style="float: right;" width="300px" />
            
            <!-- Yandex.Metrika informer -->
            <img src="https://mc.yandex.ru/informer/29804589/3_1_FFFFFFFF_EFEFEFFF_0_pageviews" style="position:absolute; left:-9999px;" alt="Яндекс.Метрика" />
            <!-- /Yandex.Metrika informer --> 
            <!-- Yandex.Metrika counter -->
            <script type="text/javascript"> (function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter29804589 = new Ya.Metrika({ id:29804589, clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true }); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = "https://mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script>
            <noscript><div>
            <img src="https://mc.yandex.ru/watch/29804589" style="position:absolute; left:-9999px;" alt="" />
            </div></noscript>
            <!-- /Yandex.Metrika counter -->
            
            <script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', 'UA-62017764-1', 'auto');ga('send', 'pageview');</script>
            
            <!-- Rating@Mail.ru counter --><script type="text/javascript">var _tmr = _tmr || [];_tmr.push({id: "2647044", type: "pageView", start: (new Date()).getTime()});(function (d, w, id) {  if (d.getElementById(id)) return;  var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;  ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";  var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};  if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }})(document, window, "topmailru-code");</script><noscript><div style="position:absolute;left:-10000px;"><img src="//top-fwz1.mail.ru/counter?id=2647044;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" /></div></noscript><!-- //Rating@Mail.ru counter --><!-- Rating@Mail.ru logo --><a href="http://top.mail.ru/jump?from=2647044"><img src="//top-fwz1.mail.ru/counter?id=2647044;t=479;l=1" style="position:absolute; left:-9999px; border:0;" alt="Рейтинг@Mail.ru" /></a><!-- //Rating@Mail.ru logo -->
            
            <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5562006825131d71" async="async"></script>
        </xsl:otherwise>
    </xsl:choose>
</xsl:variable>

<!-- loc: Префик для ссылок в карте сайта. Концевой слеш - обязательно (если $PAGES_TO_SUBFOLDER != '/' ) -->
<xsl:variable name="DOMAIN_PREFIX">
    <!-- Набор настроек в зависимости от профиля компиляции -->
    <xsl:choose>
        <xsl:when test="$CMN_USE_CONF = '2-ukr'">http://biochem-love.pp.ua/</xsl:when>
        <xsl:otherwise>http://biochem-love.appspot.com/</xsl:otherwise>
    </xsl:choose>

</xsl:variable>






<!-- помещать страницы в отдельную подпапку. Если в одной папке - пусто -->
<xsl:variable name="PAGES_TO_SUBFOLDER">part1/</xsl:variable>


<!-- Имя титульной страницы с содержанием -->
<xsl:variable name="PAGE_TITLE_TOC_NAME">index.html</xsl:variable>

<!-- lastmod: Дата и время последних изменений для элементов в карте сайта (по умолчанию) -->
<xsl:variable name="SITEMAP_DATETIME_DEFAULT">2015-09-30T00:00:00+00:00</xsl:variable>
<!-- changefreq -->
<xsl:variable name="SITEMAP_CHANGEFREQ">monthly</xsl:variable>
<!-- priority -->
<xsl:variable name="SITEMAP_PRIORITY">1</xsl:variable>
	 
	 
	<!-- Шаблон для генерации содержания в виде списка ol или ul -->
    <xsl:template name = "UT_GEN_TOC_UL" >
        <xsl:param name = "OUTLINE_CONTAINERS" />
        <xsl:param name = "LIST_TYPE" />
		<xsl:param name = "HREF_TARGET" />
        <xsl:choose>
            <xsl:when test="$LIST_TYPE = 'ol'">
                <ol>
				<xsl:for-each select="$OUTLINE_CONTAINERS">
					<li>
                        <xsl:choose>
                            <xsl:when test="$CMN_USE_EMACS = '24.4'">
                                <a href="{$DOMAIN_PREFIX}{$PAGES_TO_SUBFOLDER}page{position()}.html" target="{$HREF_TARGET}">
                                    <xsl:value-of select="./xhtml:h2[starts-with(@id, 'sec-')]/text()" />
                                </a>
                            </xsl:when>
                            <xsl:when test="$CMN_USE_EMACS = '24.5'">
                                <!-- C версии Emacs 24.5 обнаружено новые ID для h2 ( orgheadline1 ) заголовков и элементов в TOC ( #orgheadline1 ) -->
                                <a href="{$DOMAIN_PREFIX}{$PAGES_TO_SUBFOLDER}page{position()}.html" target="{$HREF_TARGET}">
                                    <xsl:value-of select="./xhtml:h2[starts-with(@id, 'orgheadline')]/text()" />
                                </a>
                            </xsl:when>
                            <xsl:otherwise>
                                <a href="{$DOMAIN_PREFIX}{$PAGES_TO_SUBFOLDER}page{position()}.html" target="{$HREF_TARGET}">
                                    <xsl:value-of select="./xhtml:h2[starts-with(@id, 'sec-')]/text()" />
                                </a>
                            </xsl:otherwise>
                        </xsl:choose>
					</li>
				</xsl:for-each>
				</ol>
            </xsl:when>
            <xsl:otherwise>
                <ul>            
				<xsl:for-each select="$OUTLINE_CONTAINERS">
					<li>
                            <xsl:choose>
                                <xsl:when test="$CMN_USE_EMACS = '24.5'">
                                    <a href="{$DOMAIN_PREFIX}{$PAGES_TO_SUBFOLDER}page{position()}.html" target="{$HREF_TARGET}">
                                        <xsl:value-of select="./xhtml:h2[starts-with(@id, 'orgheadline')]/text()" />
                                    </a>
                                </xsl:when>
                                <xsl:otherwise>
                                    <a href="{$DOMAIN_PREFIX}{$PAGES_TO_SUBFOLDER}page{position()}.html" target="{$HREF_TARGET}">
                                        <xsl:value-of select="./xhtml:h2[starts-with(@id, 'sec-')]/text()" />
                                    </a>
                                </xsl:otherwise>
                            </xsl:choose> 
					</li>
				</xsl:for-each>
				</ul>
            </xsl:otherwise>
        </xsl:choose>
	</xsl:template>
	
	<!-- Шаблон для генерации выкатывающегося содержания в стиле org-mode -->
	<xsl:template name = "UT_APPEND_HIDE_TOC" >
		<xsl:param name = "OUTLINE_CONTAINERS" />
		<xsl:param name = "HREF_TARGET" />
		
		<div id="table-of-contents">
			<h2><xsl:value-of select="$CMN_USER_HEADER_TOC_TEXT" /></h2>
			<div id="text-table-of-contents">			
				<xsl:call-template name="UT_GEN_TOC_UL">
					<xsl:with-param name="OUTLINE_CONTAINERS" select = "$OUTLINE_CONTAINERS" />
					<xsl:with-param name="LIST_TYPE">ol</xsl:with-param>
					<xsl:with-param name="HREF_TARGET" select = "$HREF_TARGET" />
				</xsl:call-template>
			</div>
		</div>
	</xsl:template>
	
	<!-- Шаблон для генерации шапки -->
	<xsl:template name = "UT_HEADER" >
		<xsl:param name = "USER_CELL_HTML" />
		<xsl:param name = "TOC_CELL_SHOW_BOOL" />
		<xsl:param name = "NEXT_CELL_SHOW_BOOL" />
		<xsl:param name = "PREV_CELL_SHOW_BOOL" />
		<xsl:param name = "CMN_TOTAL_PAGES_COUNT" />
		<xsl:param name = "CUR_POSITION" />
        <xsl:param name = "IS_TITLE_TOC_BOOL" />
		
		<table id="header" class="navbar" style="border:none;width:100%;font-size: 1.2em;">
			<tbody>
				<tr>
					<xsl:if test="$PREV_CELL_SHOW_BOOL = 1">
						<td id="header-table-prev-page-link">
							<xsl:if test="($CMN_TOTAL_PAGES_COUNT &gt; 1) and ($CUR_POSITION &gt; 1)">
								<a href="{$DOMAIN_PREFIX}{$PAGES_TO_SUBFOLDER}page{$CUR_POSITION - 1}.html"><xsl:copy-of select="$CMN_USER_HEADER_PREV_TEXT" /></a>
							</xsl:if>
						</td>
					</xsl:if>
					
					<xsl:choose>
						<xsl:when test="$NEXT_CELL_SHOW_BOOL = 1">
							<td id="header-table-next-page-link">
								<xsl:if test="$CMN_TOTAL_PAGES_COUNT &gt; $CUR_POSITION">
									<a href="{$DOMAIN_PREFIX}{$PAGES_TO_SUBFOLDER}page{$CUR_POSITION + 1}.html"><xsl:copy-of select="$CMN_USER_HEADER_NEXT_TEXT" /></a>
								</xsl:if>
							</td>
						</xsl:when>
						<xsl:when test="$NEXT_CELL_SHOW_BOOL = 2">
							<td id="header-table-next-page-link">
								<xsl:if test="$CMN_TOTAL_PAGES_COUNT &gt; 0">
									<a href="{$DOMAIN_PREFIX}{$PAGES_TO_SUBFOLDER}page1.html"><xsl:copy-of select="$CMN_USER_HEADER_NEXT_TEXT" /></a>
								</xsl:if>
							</td>
						</xsl:when>
						<xsl:otherwise>
						</xsl:otherwise>
					</xsl:choose>			
					<xsl:if test="$TOC_CELL_SHOW_BOOL = 1">
						<td id="header-table-toc-link">
							<a href="{$DOMAIN_PREFIX}{$PAGE_TITLE_TOC_NAME}"><xsl:copy-of select="$CMN_USER_HEADER_TOC_TEXT" /></a>
						</td>
					</xsl:if>					
					
					<xsl:if test="$USER_CELL_HTML">
						<td id="header-table-user-text">
							<xsl:copy-of select="$USER_CELL_HTML" />
						</td>
					</xsl:if>
				</tr>

                <tr>
                    <td>
                        <xsl:choose>
                            <xsl:when test="($CMN_USER_HEADER_READ_ALT_LANG_CAPTION != '') and ($CMN_USER_HEADER_READ_ALT_LANG_DOMAIN != '')">
                                <tr>
                                    <td style="text-align: right; padding-right: 7%;" colspan="4">
                                        <span style="background-color: rgba(233, 51, 51, 0.4); padding: 2px; margin: 10px; border-radius: 3px; text-shadow: 1px 1px black; box-shadow: 1px 1px black;">
                                            <xsl:choose>
                                                <xsl:when test="$IS_TITLE_TOC_BOOL = 1">
                                                    <!-- Внимательно быть с данной ссылкой-->
                                                    <a href="{$CMN_USER_HEADER_READ_ALT_LANG_DOMAIN}{$PAGE_TITLE_TOC_NAME}" style="color: #DCDCCC;"><xsl:copy-of select="$CMN_USER_HEADER_READ_ALT_LANG_CAPTION" /></a>
                                                </xsl:when>
                                                <xsl:otherwise>
                                                    <a href="{$CMN_USER_HEADER_READ_ALT_LANG_DOMAIN}{$CMN_USER_HEADER_READ_ALT_LANG_RELPATH}page{$CUR_POSITION}.html" style="color: #DCDCCC;"><xsl:copy-of select="$CMN_USER_HEADER_READ_ALT_LANG_CAPTION" /></a>
                                                </xsl:otherwise>
                                            </xsl:choose>
                                        </span>
                                    </td>
                                </tr>
                            </xsl:when>
                        </xsl:choose>
                    </td>
                </tr>
			</tbody>
		</table>
	</xsl:template>
	
	<!-- <xsl:template match = "//xhtml:span[matches(@class, 'section-number-[\d]+')]" />	 -->
	<xsl:template match = "/*/xhtml:body//xhtml:span[starts-with(@class, 'section-number-')]" />
	
	<!-- Генерация содержания, каждой отдельной страницы и карты сайта -->
	<xsl:template match = "/" >
		<!-- Вывод содержания toc -->
		<!-- !!! http://www.w3.org/TR/2007/REC-xslt20-20070123/#creating-result-trees @Example: "Multiple Result Documents" -->
		<xsl:result-document href="{$PAGE_TITLE_TOC_NAME}" method="html" encoding="utf-8" indent="yes" >
			<html
				xmlns="http://www.w3.org/1999/xhtml">
				<head>
					<title><xsl:copy-of select="$CMN_USER_HEADER_TOC_TEXT" /> | 
						<xsl:value-of select="$CMN_TITLE_TAG" />
					</title>
					<meta charset="utf-8" />
					<xsl:copy-of select="$CMN_META_TAGS" />
					<xsl:copy-of select="$CMN_LINKS_TAGS" />
					<xsl:copy-of select="$CMN_HEAD_STYLES_TAGS" />
					<xsl:copy-of select="$CMN_USER_HEAD_TAG" />
					<xsl:copy-of select="$CMN_HEAD_SCRIPTS_TAGS" />
					<xsl:copy-of select="$CMN_META_KEYW_TAG" />
					<xsl:copy-of select="$CMN_META_DESC_TAG" />
				</head>
				<body>
					<!-- <xsl:copy-of select="$CMN_BODY_FIRST_TAGS" /> -->
					<xsl:call-template name="UT_HEADER">
							<xsl:with-param name = "USER_CELL_HTML" select="$CMN_USER_HEADER_TEXT" />
							<xsl:with-param name = "TOC_CELL_SHOW_BOOL">0</xsl:with-param>
							<xsl:with-param name = "NEXT_CELL_SHOW_BOOL">2</xsl:with-param>
							<xsl:with-param name = "PREV_CELL_SHOW_BOOL">0</xsl:with-param>
							<xsl:with-param name = "CMN_TOTAL_PAGES_COUNT" select="$CMN_TOTAL_PAGES_COUNT" />
							<xsl:with-param name = "CUR_POSITION" select="position()" />
                            <xsl:with-param name = "IS_TITLE_TOC_BOOL">1</xsl:with-param>
					</xsl:call-template>	
					<div id="content">		
						<xsl:copy-of select="$CMN_USER_TITLEPAGE_INTRO" />
						<div class="container">
                            <div class="main-left">
                                <h2 class="title"><xsl:copy-of select="$CMN_USER_HEADER_TOC_TEXT" /></h2>
                                <div>
                                    <xsl:copy-of select="$CMN_USER_TITLE_TOC_UL_FIRST_ELEMENT" />
                                    <xsl:call-template name="UT_GEN_TOC_UL">
                                        <xsl:with-param name="OUTLINE_CONTAINERS" select = "/*/xhtml:body//xhtml:div[starts-with(@id, 'outline-container')]" />
                                        <xsl:with-param name = "LIST_TYPE">ol</xsl:with-param>
                                        <xsl:with-param name="HREF_TARGET">_blank</xsl:with-param>
                                    </xsl:call-template>						
                                </div>
                            </div>
                        </div>
						<xsl:copy-of select="$CMN_USER_TITLEPAGE_FOOTER" />
					</div>
					<xsl:copy-of select="$CMN_USER_FOOTER" />
				</body>
			</html>
		</xsl:result-document>
		
		<!-- Генерация отдельных страниц по заголовкам первого уровня экспортированных из ORG в HTML файлов -->
		<xsl:for-each select="/*/xhtml:body//xhtml:div[starts-with(@id, 'outline-container')]">
			<xsl:result-document href="{$PAGES_TO_SUBFOLDER}page{position()}.html" method="html" encoding="utf-8" indent="yes" standalone="yes">
			<html
				xmlns="http://www.w3.org/1999/xhtml">
			<!-- <xsl:variable name="PAGE_NAME">
				<xsl:value-of select="./xhtml:h2[starts-with(@id, 'sec-')]/text()" />
			</xsl:variable>
			<xsl:result-document href="{$DOMAIN_PREFIX}{$PAGES_TO_SUBFOLDER}{position()}-{translate($PAGE_NAME, 'г', 'g')}.html" method="html" encoding="utf-8" indent="yes" standalone="yes"> -->
				<head>
					<title>
                    
                        <xsl:choose>
                            <xsl:when test="$CMN_USE_EMACS = '24.5'">
                                <xsl:value-of select="./xhtml:h2[starts-with(@id, 'orgheadline')]/text()" /> | <xsl:value-of select="$CMN_TITLE_TAG" />
                            </xsl:when>
                            <xsl:otherwise>
                                <xsl:value-of select="./xhtml:h2[starts-with(@id, 'sec-')]/text()" /> | <xsl:value-of select="$CMN_TITLE_TAG" />
                            </xsl:otherwise>
                        </xsl:choose> 
					</title>
					<meta charset="utf-8" />
					<xsl:copy-of select="$CMN_META_TAGS" />
					<xsl:copy-of select="$CMN_LINKS_TAGS" />
					<xsl:copy-of select="$CMN_HEAD_STYLES_TAGS" />
					<xsl:copy-of select="$CMN_USER_HEAD_TAG" />
					<xsl:copy-of select="$CMN_HEAD_SCRIPTS_TAGS" />
					<xsl:copy-of select="$CMN_META_KEYW_TAG" />
				</head>
				<body>
					<!-- <xsl:copy-of select="$CMN_BODY_FIRST_TAGS" /> -->
					<xsl:call-template name="UT_HEADER">
							<xsl:with-param name = "USER_CELL_HTML" select="$CMN_USER_HEADER_TEXT" />
							<xsl:with-param name = "TOC_CELL_SHOW_BOOL">1</xsl:with-param>
							<xsl:with-param name = "NEXT_CELL_SHOW_BOOL">1</xsl:with-param>
							<xsl:with-param name = "PREV_CELL_SHOW_BOOL">1</xsl:with-param>
							<xsl:with-param name = "CMN_TOTAL_PAGES_COUNT" select="$CMN_TOTAL_PAGES_COUNT" />
							<xsl:with-param name = "CUR_POSITION" select="position()" />
                            <xsl:with-param name = "IS_TITLE_TOC_BOOL">0</xsl:with-param>
					</xsl:call-template>	
					<div id="content">
						<!-- Вставка скрывающегося меню -->
						<xsl:call-template name="UT_APPEND_HIDE_TOC">
							<xsl:with-param name="OUTLINE_CONTAINERS" select = "/*/xhtml:body//xhtml:div[starts-with(@id, 'outline-container')]" />
							<xsl:with-param name="HREF_TARGET">_self</xsl:with-param>
						</xsl:call-template>
						
						<!-- Содержимое страницы - контент -->
						<!-- <xsl:apply-templates select="./xhtml:h2[matches(@class, 'sec-[\d]+')]" /> -->
						<xsl:copy-of select="." />
						
						<!-- Пользовательский футер -->
						<xsl:copy-of select="$CMN_USER_FOOTER" />
					</div>
				</body>
			</html>
			</xsl:result-document>
		</xsl:for-each>
		<xsl:result-document href="sitemap_add.xml" method="xml" encoding="utf-8" >
		<!-- <urlset xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
				xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd"
				xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"> -->
			<url>
				<loc><xsl:value-of select="$DOMAIN_PREFIX" /><xsl:value-of select="$PAGE_TITLE_TOC_NAME" /></loc>
				<lastmod><xsl:value-of select="$SITEMAP_DATETIME_DEFAULT" /></lastmod>
				<changefreq><xsl:value-of select="$SITEMAP_CHANGEFREQ" /></changefreq>
				<priority><xsl:value-of select="$SITEMAP_PRIORITY" /></priority>
			</url>
			
			<xsl:for-each select="/*/xhtml:body//xhtml:div[starts-with(@id, 'outline-container')]">
				<url>
					<loc><xsl:value-of select="$DOMAIN_PREFIX" /><xsl:value-of select="$PAGES_TO_SUBFOLDER" />page<xsl:value-of select="position()" />.html</loc>
					<lastmod><xsl:value-of select="$SITEMAP_DATETIME_DEFAULT" /></lastmod>
					<changefreq><xsl:value-of select="$SITEMAP_CHANGEFREQ" /></changefreq>
					<priority><xsl:value-of select="$SITEMAP_PRIORITY" /></priority>
				</url>
			</xsl:for-each>
		<!-- </urlset> -->
	</xsl:result-document>
	</xsl:template>
	
</xsl:stylesheet> 
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

# ������������� v1.4 ��� v2.0 - � options

function MathMLSupport_autoload()
{    
	mso_hook_add('head', 'MathMLSupport_head');
	mso_hook_add('body_end', 'MathMLSupport_body_end');
	# $options = mso_get_option('plugin_MathMLSupport', 'plugins', array());
}


function MathMLSupport_uninstall($args = array())
{
    mso_delete_option('plugin_MathMLSupport', 'plugins' );
    return $args;
}

function MathMLSupport_mso_options()
{
    mso_admin_plugin_options('plugin_MathMLSupport', 'plugins',
        array(
            'MMLVersion' => array(
                'type' => 'select',
				'name' => t('������ ASCIIMathML.js'),
				'description' => t('ASCIIMathML v1.4 ����� "�����"'),
				'values' => 'v1.4.1 # v2.1',
                'default' => 'v1.4.1'
            ),
		'���������� � ������� - MathMLSupport', 
		'����� ����� �����'
    );
}

function MathMLSupport_head($arg = array())
{
    $options = mso_get_option('plugin_MathMLSupport', 'plugins', array());

	$MMLjs_path = getinfo('plugins_url') . 'MathMLSupport/ASCIIMathML_' . ((isset($options['MMLVersion'])) ? $options['MMLVersion'] : 'v1.4.1') . '.js';
	
    echo '
	<script src="' . $MMLjs_path . '"></script>
	<script src="' . getinfo('plugins_url') . 'unimath.js"></script>
	';

    return $arg;
}

# ������ pre � ������
function body_end_content($arg = array())
{
		echo '<script type="text/javascript">
				mMathTextToHtml();
			  </script>';
        return $arg;
}
?>
require([
    "dojo/aspect",
    "dojo/parser",
    "dijit/registry",
    "dojo/dom",
    "dijit/layout/BorderContainer",
    "dijit/layout/TabContainer",
    "dijit/layout/ContentPane",
    "dijit/form/Button", 
    "dojo/domReady!"
], function( aspect, parser, registry, dom ){
    parser.parse();

    // ! Create molecular editor
    new JSDraw( "qusJSME", 
                { width: 800, height: 400 } );
    
    // ! Create second spreadsheet for performing basic chemical equations and database searches
    var qusJSSS2 = new JSDraw2.Table( "", 
        {
        searchable: true, 
            columns: [
                { key: 'compid',        caption: 'Comp ID',         type: "compid",         width: 120  },
                { key: 'compstructure', caption: 'Comp Structure',  type: 'compstructure',   width: 200 },
                { key: 'mf',            caption: 'MF',              type: 'mf'                          },
                { key: 'mw',            caption: 'MW',              type: 'mw'                          },
                ]
        }, "qusJSSS2");
        
    // insert first row as an example
    qusJSSS2.insert( false, { compid: "serotonine" } );
    
    // ! Columns of main spreadsheet
    var qusJSSS1_Cols = [
        { key: '[structure]',   caption: 'Structure' },
        { key: 'mf',            caption: 'Mol. Formula',    width: 50,  type: 'mf',     viewonly: true  },
        { key: 'mw',            caption: 'Mol. Weight',     width: 50,  type: 'mw',     viewonly: true  },
        { key: 'smiles',        caption: 'Smiles',          width: 200, type: "smiles", viewonly: true  },
        { key: 'UBA',           caption: 'Exp. Value',      width: 100, type: "float",  viewonly: false }        
    ];
    
    // ! Create main JSDraw spreadsheet
    var qusJSSS1 = new JSDraw2.Table( "", 
        { 
            spreadsheet:    true, 
            pivot:          false, 
            columns:        qusJSSS1_Cols, 
            width:          '90%', 
            editable:       true, 
            searchable:     true, 
            molwidth:       200 
        }, "qusJSSS1");
        
    qusJSSS1.insert( true );
    
    // ! Hack to make it possible to opend and append data via custom form (befor works only from Scilligence webserver)
    if ( null == JSDraw2.Table.openDlg ) {
        var a = scil.Utils.createElement( null, "div" );
        var b = scil.Utils.createElement( a, "textarea", null, {
                width: "580px",
                height: "400px"
            });
        var d = scil.Utils.createElement( a, "div", null, {
                textAlign: "center"
            });
        var c = scil.Utils.createElement( d, "button", "Open", {
                width: scil.Utils.buttonWidth + "px"
            });
        var f = scil.Utils.createElement( d, "button", "Append", {
                width: scil.Utils.buttonWidth + "px"
            });
            
        JSDraw2.Table.openDlg = new JSDraw2.Dialog( "<img src='" + scil.Utils.imgSrc( "img/open.gif" ) + "'>Open File", a );
        
        dojo.connect( c, "onClick", function( e ) {
            JSDraw2.Table.openDlg.jss.openSdf( true );
            JSDraw2.Table.saveDlg.hide();
            e.preventDefault()
        });
        
        dojo.connect( f, "onClick", function( e ) {
            JSDraw2.Table.openDlg.jss.openSdf( false );
            JSDraw2.Table.saveDlg.hide();
            e.preventDefault()
        });
        
        // append created form to the first spreadsheet
        JSDraw2.Table.openDlg.txt = b;
        JSDraw2.Table.openDlg.jss = JSDraw2.Table.get( "qusJSSS1" );
        JSDraw2.Table.openDlg.txt.value = "";
    }
    
  
      
    aspect.after( registry.byId( "btnGetMolSDF" ), "onClick", 
        function(){ 
            dom.byId('txtJSME2FuncResult').value = JSDraw.get("qusJSME").getMolfile(); 
        }
    );
    
    aspect.after( registry.byId( "btnSetMolSDF" ), "onClick", 
        function(){ 
            JSDraw.get("qusJSME").setMolfile(dom.byId('txtJSME2FuncResult').value); 
        }
    );
    
    aspect.after( registry.byId( "btnGetMolXML" ), "onClick", 
        function(){ 
            dom.byId('txtJSME2FuncResult').value = JSDraw.get("qusJSME").getXml(); 
        }
    );
    
    aspect.after( registry.byId( "btnSetMolXML" ), "onClick", 
        function(){ 
            JSDraw.get("qusJSME").setXml(dom.byId('txtJSME2FuncResult').value); 
        }
    );
    
    aspect.after( registry.byId( "btnGetMolSMI" ), "onClick", 
        function(){ 
            dom.byId('txtJSME2FuncResult').value = JSDraw.get("qusJSME").getSmiles();
        }
    );
    
    aspect.after( registry.byId( "btnGetMolBrutto" ), "onClick", 
        function(){ 
            dom.byId('txtJSME2FuncResult').value = JSDraw.get("qusJSME").getFormula();
        }
    );
    
    aspect.after( registry.byId( "btnCalcMolMW" ), "onClick", 
        function(){ 
            dom.byId('txtJSME2FuncResult').value = JSDraw.get("qusJSME").getMolWeight();
        }
    );
    
});
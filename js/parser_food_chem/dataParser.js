/*
http://msdn.microsoft.com/ru-ru/library/ie/d8ez24f2(v=vs.94).aspx
*/
var divProgress;
// var regEx_getProdValName = /([A-z\d,%\(\) \-]+) Production by Country in ([A-z\d,%\(\) \-]+)/g;
var regEx_getProdValName = /([\w\d\s,%()\.&-]+) Production by Country in ([\w\d%\s,%\.()&-]+)/;
var regEx_getValString = /(([0-9]+),[ ]?)?([0-9]+).[ ]?([0-9]+)/g;
var regEx_getProdYear = /Year of Estimate:[ ]?([\d]+)/;
var productsProgressCounter = 0; // for displaying progress bar
var productsProgressTotal = 0; // for displaying progress bar
var	countriesNamesArray = []; // [country's index] = name
var countriesDataArray = []; // [country's index][product's index] = val
var countriesRanksArray = []; // [country's index][product's index] = rank
var productsNamesArray = []; // [product's index] = prod. name
var productsUnitsArray = []; // [product's index] = prod. unit // kg, MT ...
var productsYearArray = []; // [product's index] = year
var productsCountrNumArray = []; // [product's index] = number of countries in the table
var countriesNameTransl = [
							['United States','Соединенные Штаты'],
							['EU-27','ЕС-27'],
							['Australia','Австралия'],
							['Turkey','Турция'],
							['Chile','Чили'],
							['China','Китай'],
							['India','Индия'],
							['Iran, Islamic Republic Of','Иран, Исламская Республика'],
							['Italy','Италия'],
							['Japan','Япония'],
							['Korea, Republic Of','Корея, Республика'],
							['Mexico','Мексика'],
							['Malaysia','Малайзия'],
							['Norway','Норвегия'],
							['Russian Federation','Русской Федерации'],
							['Algeria','Алжир'],
							['Spain','Испания'],
							['Canada','Канада'],
							['Switzerland','Швейцария'],
							['Greece','Греция'],
							['Hong Kong','Гонконг'],
							['Taiwan, Province Of China','Тайвань, провинция Китая'],
							['Viet Nam','Вьетнам'],
							['United Arab Emirates','Объединенные Арабские Эмираты'],
							['Brazil','Бразилия'],
							['Argentina','Аргентина'],
							['Colombia','Колумбия'],
							['New Zealand','Новая Зеландия'],
							['Uruguay','Уругвай'],
							['Ukraine','Украина'],
							['Egypt','Египет'],
							['Belarus','Беларусь'],
							['Venezuela','Венесуэла'],
							['Morocco','Марокко'],
							['Kazakhstan','Казахстан'],
							['Ethiopia','Эфиопия'],
							['Iraq','Ирак'],
							['Syrian Arab Republic','Сирийская Арабская Республика'],
							['Azerbaijan','Азербайджан'],
							['Afghanistan ','Афганистан'],
							['Tunisia','Тунис'],
							['Kyrgyzstan','Киргизия'],
							['Serbia','Сербии'],
							['South Africa','ЮАР'],
							['Pakistan','Пакистан'],
							['Uzbekistan','Узбекистан'],
							['Peru','Перу'],
							['Moldova, Republic Of','Молдова'],
							['Armenia','Армения'],
							['Macedonia, The Former Yugoslav Republic Of','Македония, бывшая Югославская Республика'],
							['Libya','Ливия'],
							['Tajikistan','Таджикистан'],
							['Kenya','Кения'],
							['Bolivia','Боливия'],
							['Bosnia and Herzegovina','Босния и Герцеговина'],
							['Ecuador','Эквадор'],
							['Lebanon','Ливан'],
							['Zimbabwe','Зимбабве'],
							['Nepal','Непал'],
							['Eritrea','Эритрее'],
							['Georgia','Грузия'],
							['Yemen','Йемен'],
							['Turkmenistan','Туркменистан'],
							['Jordan','Иордания'],
							['Albania','Албания'],
							['Bangladesh','Бангладеш'],
							['Bhutan','Бутан'],
							['Serbia and Montenegro','Сербии и Черногории'],
							['Bulgaria','Болгария'],
							['Kuwait','Кувейт'],
							['Israel','Израиль'],
							['EU-25','ЕС-25'],
							['Saudi Arabia','Саудовская Аравия'],
							['Romania','Румыния'],
							['Paraguay','Парагвай'],
							['Philippines','Филиппины'],
							['Nicaragua','Никарагуа'],
							['Costa Rica','Коста-Рика'],
							['Angola','Ангола'],
							['Guatemala','Гватемала'],
							['Senegal','Сенегал'],
							['Dominican Republic','Доминиканская Республика'],
							['Côte D\'ivoire','Берег Слоновой Кости'],
							['Honduras','Гондурас'],
							['Ghana','Гана'],
							['El Salvador','Сальвадор'],
							['Jamaica','Ямайка'],
							['Oman','Оман'],
							['Congo','Конго'],
							['Gabon ','Габон'],
							['Singapore','Сингапур'],
							['France','Франция'],
							['Thailand','Таиланд'],
							['Indonesia','Индонезия'],
							['Cuba','Куба'],
							['Sudan','Судан'],
							['Swaziland','Свазиленд'],
							['Mauritius','Маврикий'],
							['Zambia','Замбия'],
							['Mozambique','Мозамбик'],
							['Uganda','Уганда'],
							['Malawi','Малави'],
							['Tanzania, United Republic Of','Танзания, Объединенная Республика'],
							['Guyana','Гайана'],
							['Croatia','Хорватия'],
							['Fiji','Фиджи'],
							['Panama','Панама'],
							['Myanmar','Мьянма'],
							['Cameroon','Камерун'],
							['Belize','Белиз'],
							['Congo, The Democratic Republic Of The','Конго, Демократическая Республика'],
							['Madagascar','Мадагаскар'],
							['Nigeria','Нигерия'],
							['Papua New Guinea','Папуа-Новая Гвинея'],
							['Sri Lanka','Шри Ланка'],
							['Mali','Мали'],
							['Chad','Чад'],
							['Burkina Faso','Буркина-Фасо'],
							['Barbados','Барбадос'],
							['Guinea','Гвинея'],
							['Somalia','Сомали'],
							['French West Indies','Французская Вест-Индия'],
							['Burundi','Бурунди'],
							['Niger','Нигер'],
							['Rwanda','Руанда'],
							['Benin','Бенин'],
							['Togo','Того'],
							['Sierra Leone','Сьерра-Леоне'],
							['Suriname','Суринам'],
							['Seychelles','Сейшельские острова'],
							['Tonga','Тонга'],
							['Trinidad and Tobago','Тринидад и Тобаго'],
							['Vanuatu','Вануату'],
							['Samoa','Самоа'],
							['Liberia','Либерия'],
							['Maldives','Мальдивы'],
							['Mongolia','Монголия'],
							['Macao','Макао'],
							['Mauritania','Мавритания'],
							['New Caledonia','Новая Каледония'],
							['Qatar','Катар'],
							['Netherlands Antilles','Нидерландские Антильские острова'],
							['Bermuda','Бермудские острова'],
							['Brunei Darussalam','Бруней-Даруссалам'],
							['Bahrain','Бахрейн'],
							['Central African Republic','Центрально-Африканская Республика'],
							['Bahamas','Багамские острова'],
							['Botswana','Ботсвана'],
							['Cape Verde','Кабо-Верде'],
							['Djibouti','Джибути'],
							['Cambodia','Камбоджа'],
							['Comoros','Коморские острова'],
							['Saint Kitts and Nevis','Сент-Китс и Невис'],
							['Korea, Democratic People\'s Republic Of','Корейская Народно-Демократическая Народная Республика'],
							['Lao People\'s Democratic Republic ','Лаосская Народно-Демократическая Республика'],
							['Iceland','Исландия'],
							['Grenada','Гренада'],
							['Gambia','Гамбия'],
							['Haiti','Гаити'],
							['Guinea-Bissau','Гвинея-Бисау'],
							['Poland','Польша'],
							['Hungary','Венгрия'],
							['Germany','Германия'],
							['Solomon Islands','Соломоновы острова'],
							['Sao Tome and Principe','Сан-Томе и Принсипи'],
							['Lesotho','Лесото'],
							['Namibia','Намибия'],
							['Austria','Австрия'],
							['Belgium-Luxembourg','Бельгия-Люксембург'],
							['Estonia','Эстония'],
							['Cyprus','Кипр'],
							['Czech Republic','Чешская республика'],
							['Denmark','Дания'],
							['Lithuania','Литва'],
							['Latvia','Латвия'],
							['United Kingdom','Великобритания'],
							['Ireland','Ирландия'],
							['Sweden','Швеция'],
							['Slovenia','Словения'],
							['Slovakia','Словакия'],
							['Portugal','Португалия'],
							['Netherlands','Нидерланды'],
							['Faroe Islands','Фарерские острова'],
							['Belgium (without Luxembourg)','Бельгия (без Люксембург)'],
							['Equatorial Guinea','Экваториальная Гвинея'],
							['Réunion','Реюньон'],
							['Other','Другие страны'],
							['Montenegro','Черногория'],
						  ];
var productsNameTransl = [
							['Almonds, Shelled Basis','Миндаль, очищенные ядра'],
							['Animal Numbers, Cattle','Численность крупного рогатого скота'],
							['Animal Numbers, Swine','Поголовье свиней'],
							['Barley','Ячмень'],
							['Beef and Veal Meat','Говядина и телятина'],
							['Broiler Meat (Poultry)','Мяса птицы - бройлеров'],
							['Canned Peaches','Консервированные персики'],
							['Canned Pears','Консервированные груши'],
							['Canned Tomatoes','Консервированные помидоры'],
							['Centrifugal Sugar','Центрофугированный сахар'],
							['Coconut Oil','Кокосовое масло'],
							['Concentrated Apple Juice','Концентрированный яблочный сок'],
							['Copra Meal','Шрот копры'],
							['Copra Oilseed','Масло копры'],
							['Corn','Кукуруза'],
							['Cotton','Хлопок'],
							['Cottonseed Meal','Хлопковый шрот'],
							['Cottonseed Oil','Хлопковое масло'],
							['Cottonseed Oilseed','Масло семян хлопка'],
							['Dairy, Butter','Молочные продукты, масло'],
							['Dairy, Cheese','Молочные продукты, сыр'],
							['Dairy, Dry Whole Milk Powder','Порошок сухого цельного молока'],
							['Dairy, Milk, Fluid','Молоко (жидкость)'],
							['Dairy, Milk, Nonfat Dry','Обезжиренное сухое молоко'],
							['Durum Wheat','Твердая пшеница'],
							['Filberts, Inshell Basis','Фундук в скорлупе (неочищенный)'],
							['Fish Meal','Рыбный шрот'],
							['Fresh Apples','Свежие яблоки'],
							['Fresh Apricots','Свежие абрикосы'],
							['Fresh Asparagus','Свежая спаржа'],
							['Fresh Avocados','Свежий авокадо'],
							['Fresh Cherries, (Sweet & Sour)','Свежая вишня (сладкая и кислая)'],
							['Fresh Citrus, Other','Свежие цитрусовые, остальные'],
							['Fresh Grapefruit','Свежий грейпфрут'],
							['Fresh Lemons','Свежие лимоны'],
							['Fresh Oranges','Свежие апельсины'],
							['Fresh Peaches & Nectarines','Свежие персики и нектарин'],
							['Fresh Pears','Свежие груши'],
							['Fresh Plums & Prunes','Свежие сливы и чернослив'],
							['Fresh Strawberries','Свежая клубника'],
							['Fresh Table Grapes','Свежий столовый виноград'],
							['Fresh Tangerines','Свежие мандарины'],
							['Frozen Potato Products','Замороженные продукты из картофеля'],
							['Frozen Strawberries','Замороженная клубника'],
							['Grapefruit Juice','Грейпфрутовый сок'],
							['Green Coffee','Зеленый кофе'],
							['Kiwifruit','Киви'],
							['Lemon Juice','Лимонный сок'],
							['Macadamia, Inshell Basis','Макадамия - орехи в скорлупе (неочищенные)'],
							['Milled Rice','Шлифованный рис'],
							['Millet','Просо'],
							['Mixed Grain','Смешанные зерна'],
							['Oats','Овес'],
							['Olive Oil','Оливковое масло'],
							['Orange Juice','Апельсиновый сок'],
							['Palm Kernel Meal','Пальмоядровый шрот'],
							['Palm Kernel Oil','Пальмоядровое масло'],
							['Palm Kernel Oilseed','Пальмоядровое масло семян'],
							['Palm Oil','Пальмовое масло'],
							['Peanut Meal','Арахисовый шрот'],
							['Peanut Oil','Арахисовое масло'],
							['Peanut Oilseed','Арахисовое масло семян'],
							['Pecans, Inshell Basis','Пекан в скорлупе (неочищенный)'],
							['Pistachios, Inshell Basis','Фисташки в скорлупе'],
							['Prunes (Plums, Dried)','Чернослив (сливы, сушеные)'],
							['Raisins','Изюм'],
							['Rapeseed Meal','Шрот рапса'],
							['Rapeseed Oil','Рапсовое масло'],
							['Rapeseed Oilseed','Рапсовое масло семян'],
							['Rye','Рожь'],
							['Sorghum','Сорго'],
							['Soybean (Local) Meal','Соевый шрот (локальное производство)'],
							['Soybean (Local) Oil','Соевое масло(локальное производство)'],
							['Soybean Meal','Соевый шрот'],
							['Soybean Oil','Соевое масло'],
							['Soybean Oilseed','Масло семян сои'],
							['Soybean Oilseed (Local)','Масло семян сои (локальное производство)'],
							['Sunflowerseed Meal','Шрот подсолнуха'],
							['Sunflowerseed Oil','Масло подсолнечное'],
							['Sunflowerseed Oilseed','Масло семян подсолнуха'],
							['Swine Meat','Свинина'],
							['Tangerine Juice','Мандариновый сок'],
							['Tobacco, Mfg., Cigarettes','Табак, сигареты (известное производство)'],
							['Tobacco, Unmfg., Total','Табак неизвестного производста, (всего)'],
							['Tomato Paste, 28-30% TSS Basis','Томатная паста (28-30% оценка стресового тестирования)'],
							['Tomato Sauce','Томатный соус'],
							['Turkey Meat (Poultry)','Мясо индейки (мясо птицы)'],
							['Walnuts, Inshell Basis','Грецкие орехи в скорлупе'],
							['Wheat','Пшеница'],
						  ];
						  
function seachInDblArr(arr, val) {
	// var i1 = -1;
	var i2 = -1;
	
	for (var i=0; i < arr.length; i++) {
		i2 = arr[i].indexOf(val);
		if (-1 !== i2) {			
			// i1 = i;
			return i;
		}
	}
}

function convFloat(val)	{
	return val.replace(regEx_getValString, '$2$3,$4')
}

function translateProduct(bool, str) {
	if (true == bool) {		
		var idx = seachInDblArr(productsNameTransl, str);
		return -1!==idx?productsNameTransl[idx][1]:str;
	}
}

function translateCountryName(bool, str) {
	if (true == bool) {		
		var idx = seachInDblArr(countriesNameTransl, str);
		return -1!==flag?countriesNameTransl[idx][1]:str;
	}		
}

function getCountriesNames() {
	var s = '';
	for (var i = 0; i < countriesNamesArray.length; i++) {
		s += translateCountryName(true, countriesNamesArray[i]) + '\n';
	}
	alert(s);
}
					
function makeReport(translate) {
	divProgress.innerText = 'Making report ...';
	
	var productsNum = productsNamesArray.length;
	var countriesNum = countriesNamesArray.length;	
	
	// header	| Countries | Product#1 | Product#2 | ...
	var resStr = 'Products→\t';
	for (var i = 0; i < productsNum; i++) {
		resStr += translateProduct(true, productsNamesArray[i]) + '\t';		
	}	
	resStr += '\nUnits→\t';
	for (var i = 0; i < productsNum; i++) {
		resStr += productsUnitsArray[i] + '\t';		
	}	
	resStr += '\nYears→\t';
	for (var i = 0; i < productsNum; i++) {
		resStr += productsYearArray[i] + '\t';		
	}	
		
	for (var k = 0; k < countriesNum; k++) {
		
		// 2-d row Country#1 | 
		resStr += '\r\n' + translateCountryName(true, countriesNamesArray[k]) + '\t';
		
		// then values for each product
		for (var j = 0; j < productsNum; j++) {
			if ((Object.prototype.toString.call( countriesDataArray[k] ) === '[object Array]')) {
				if (typeof countriesDataArray[k][j] !== "undefined") {
					resStr += convFloat(countriesDataArray[k][j]) + '\t';
				} else {
					resStr += '-1\t';
				}
			} else {			
				resStr += '-1\t';
			}
		}
	}
	divProgress.innerText = 'Completed. \n Total string length - ' + resStr.length;
	alert(resStr);
}


function makeHTMLReport(translate, repType) {	
	divProgress.innerText = 'Making html report ...';
	
	var productsNum = productsNamesArray.length;
	var countriesNum = countriesNamesArray.length;	
	
	if (1 == repType) {
		// header	| Countries | Product#1 | Product#2 | ...
		var resStr = '<table>\n\t<tr>\n\t\t<td>Products→</td>';
		for (var i = 0; i < productsNum; i++) {
			resStr += '<td>' + translateProduct(true, productsNamesArray[i]) + '</td>';		
		}	
		resStr += '\n\t</tr>\n\t<tr>\n\t\t<td>Units→</td>';
		for (var i = 0; i < productsNum; i++) {
			resStr += '<td>' + productsUnitsArray[i] + '</td>';		
		}	
		resStr += '\n\t</tr>\n\t<tr><td>\n\t\tYears→</td>';
		for (var i = 0; i < productsNum; i++) {
			resStr += '<td>' + productsYearArray[i] + '</td>';				
		}	
		
		resStr += '\n\t</tr>';
			
		for (var k = 0; k < countriesNum; k++) {
			
			// 2-d row Country#1 | 
			resStr += '\n\t<tr>\n\t\t<td>' + translateCountryName(true, countriesNamesArray[k]) + '</td>';
			
			// then values for each product
			for (var j = 0; j < productsNum; j++) {
				if ((Object.prototype.toString.call( countriesDataArray[k] ) === '[object Array]')) {
					if (typeof countriesDataArray[k][j] !== "undefined") {
						resStr += '<td>' + convFloat(countriesDataArray[k][j]) + '</td>';
					} else {
						resStr += '<td>-1</td>';
					}
				} else {			
					resStr += '<td>-1</td>';
				}
			}
		}
		resStr += '\n\t</tr>\n</table>';
	} else if (2 == repType) {
				// header	| Country [units] - year {total number of contries} | Country#1 | Country#2 | ...
		var resStr = '<table>\n\t<tr>\n\t\t<td>Product [units] - year {num. of countr.}</td>';
		/*for (var i = 0; i < productsNum; i++) {
			resStr += '<td>' + translateProduct(true, productsNamesArray[i]) + '</td>';		
		}*/
		for (var i = 0; i < countriesNum; i++) {
			resStr += '<td>' + translateCountryName(true, countriesNamesArray[i]) + '</td>';		
		}
		resStr += '\n\t</tr>';
			
		for (var k = 0; k < productsNum; k++) {			
			// 2-d row Product#1 | 3-d Product#2 ...
			resStr += '\n\t<tr>\n\t\t<td>' + translateProduct(true, productsNamesArray[k]) + 
					  ' [' + productsUnitsArray[k] + '] - ' + productsYearArray[k] + ' {' + productsCountrNumArray[k] + '}</td>';
			
			// then values for each country
			for (var j = 0; j < countriesNum; j++) {								
				if ((Object.prototype.toString.call(countriesDataArray[j]) === '[object Array]')) {
					if (typeof countriesDataArray[j][k] !== "undefined") {
						resStr += '<td>' + convFloat(countriesDataArray[j][k]) + ' (' + countriesRanksArray[j][k]  + ')</td>';
					} else {
						resStr += '<td>-1</td>';
					}
				} else {			
					resStr += '<td>-1</td>';
				}
			}
		}
		resStr += '\n\t</tr>\n</table>';
	} else if (3 == repType) {
				// header	| Country [units] - year {total number of contries} | Country#1 | Country#2 | ...
		var resStr = '<table>\n\t<tr>\n\t\t<td>Product [units] - year {num. of countr.}</td>';
		/*for (var i = 0; i < productsNum; i++) {
			resStr += '<td>' + translateProduct(true, productsNamesArray[i]) + '</td>';		
		}*/
		for (var i = 0; i < countriesNum; i++) {
			resStr += '<td colspan=2>' + translateCountryName(true, countriesNamesArray[i]) + '</td>';		
		}
		resStr += '\n\t</tr>';
			
		for (var k = 0; k < productsNum; k++) {			
			// 2-d row Product#1 | 3-d Product#2 ...
			resStr += '\n\t<tr>\n\t\t<td>' + translateProduct(true, productsNamesArray[k]) + 
					  ' [' + productsUnitsArray[k] + '] - ' + productsYearArray[k] + ' {' + productsCountrNumArray[k] + '}</td>';
			
			// then values for each country
			for (var j = 0; j < countriesNum; j++) {								
				if ((Object.prototype.toString.call(countriesDataArray[j]) === '[object Array]')) {
					if (typeof countriesDataArray[j][k] !== "undefined") {
						resStr += '<td>' + convFloat(countriesDataArray[j][k]) + '</td><td>(' + countriesRanksArray[j][k]  + ')</td>';
					} else {
						resStr += '<td>-1</td>';
					}
				} else {			
					resStr += '<td>-1</td>';
				}
			}
		}
		resStr += '\n\t</tr>\n</table>';
	}
	
	divProgress.innerText = 'Completed. \n Total string length - ' + resStr.length;
	alert(resStr);
}

function getData(htmlDoc, flag) { // start from table > tbody
	if (1 == flag) {
		try {
			var startParent = htmlDoc.getElementById('horizontal-bar-chart');
			var startParent = startParent.children[0]; // main data table
			var numTR = startParent.childElementCount; // amount of in in table's body
			
			var tmp = htmlDoc.getElementById('lblProduct');
			var prodFull = regEx_getProdValName.exec(tmp.innerText); // Tomato Paste, 28-30% TSS Basis Production by Country in MT, Net Weight
			var prodName = prodFull[1]; // e.g. Tomato Paste, 28-30% TSS Basis
			var prodVal = prodFull[2]; // e.g. in MT, Net Weight
			
			productsNamesArray.push(prodName); // save prod. name
			var prodNum = productsNamesArray.length - 1;
			productsUnitsArray.push(prodVal); // save prod. unit	
			
			var tmp = htmlDoc.getElementById('lblDataNote');
			var prodYear = regEx_getProdYear.exec(tmp.innerText); // Year of Estimate: 2012
			productsYearArray.push(prodYear[1]); // 2012
		} catch (e) {
			alert(e.name)
		}
		
		var name =''; // e.g. Ukraine
		var rank = 0; // e.g. 60
		var val =''; // e.g. 6,710.00
		var indexC = 0; // for current array's element - country's name
		var indexP = 0; // for current array's element - production	
		var index = 0;
		
		var cn = numTR-1; // number of countries in the list - data for productsCountrNumArray[]
		productsCountrNumArray.push(cn);
		
		for (var i=1; i < numTR; i++) { // start form 1 cause first row is table's header
			rank = startParent.children[i].children[0].innerText; // get country's rank - same as var i
			name = startParent.children[i].children[1].innerText; // get country's name
			val = startParent.children[i].children[2].innerText; // get production's value
			
			// get country's number
			index = countriesNamesArray.indexOf(name); // if this country is already in array
			if (-1 == index) {
				countriesNamesArray.push(name);				
				index = countriesNamesArray.length - 1;						
				countriesDataArray[index] = [];
				countriesRanksArray[index] = [];
			}			
				
			countriesDataArray[index][prodNum] = val;		// get production's val			
			countriesRanksArray[index][prodNum] = rank;		// country's rank			
		}
	} else {
		var links = document.getElementById('gvProducts'); // tbody of table with products href
		var links = links.children[0];
		
		var productTable = document.getElementsByClassName("products");
		var newdiv = document.createElement('table');
		newdiv.innerHTML = links.innerHTML;		
	}
}

/* 
 * DOMParser HTML extension 
 * 2012-02-02 
 * 
 * By Eli Grey, http://eligrey.com 
 * Public domain. 
 * NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK. 
 */  

/*! @source https://gist.github.com/1129031 */  
/*global document, DOMParser*/  

(function(DOMParser) {  
    "use strict";  
    var DOMParser_proto = DOMParser.prototype  
      , real_parseFromString = DOMParser_proto.parseFromString;

    // Firefox/Opera/IE throw errors on unsupported types  
    try {  
        // WebKit returns null on unsupported types  
        if ((new DOMParser).parseFromString("", "text/html")) {  
            // text/html parsing is natively supported  
            return;  
        }  
    } catch (ex) {}  

    DOMParser_proto.parseFromString = function(markup, type) {  
        if (/^\s*text\/html\s*(?:;|$)/i.test(type)) {  
            var doc = document.implementation.createHTMLDocument("")
              , doc_elt = doc.documentElement
              , first_elt;

            doc_elt.innerHTML = markup;
            first_elt = doc_elt.firstElementChild;

            if (doc_elt.childElementCount === 1
                && first_elt.localName.toLowerCase() === "html") {  
                doc.replaceChild(first_elt, doc_elt);  
            }  

            return doc;  
        } else {  
            return real_parseFromString.apply(this, arguments);  
        }  
    };  
}(DOMParser));

function startParser(flag_html, repType) {
	var xmlhttp;
	if (window.XMLHttpRequest) { // code for IE7+, Firefox, Chrome, Opera, Safari
		xmlhttp = new XMLHttpRequest();
	}
	else { // code for IE6, IE5
		xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
	}
	xmlhttp.onreadystatechange = function() {
		if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {				
			var parser = new DOMParser();
			var doc = parser.parseFromString(xmlhttp.responseText, "text/html");
			getData(doc, flag);
			productsProgressCounter++;
			divProgress.innerText = 'Parsed: ' + productsProgressCounter + " / "	+ productsProgressTotal;
		}
	}
	divProgress.innerText = 'Parsing starts ...';
	flag = 1;
	var links = document.getElementById('gvProducts'); 
	var links = links.children[0]; // tbody of table with products href
	var n = links.childElementCount - 1;
	productsProgressTotal = n+1;
	
	divProgress.innerText = 'Total items to be parsed - ' + productsProgressTotal;
	
	// !!! Number of products - set here 'n'
	for (var i=1; i <= n; i++) {
		var s = links.children[i].children[0].children[0].href; // tbody[tr] > td > a.href
		xmlhttp.open("GET", s, false);
		// xmlhttp.setRequestHeader('Content-Type','text/xml');
		xmlhttp.send();		
	}
	
	if (repType > 0) {
		makeHTMLReport(flag_html, repType);
		return 0;
	} else {
		makeReport(flag_html);
		return 0;
	}
}

window.addEventListener("DOMContentLoaded", function () {		
	divProgress = document.createElement('div');
	divProgress.innerHTML = '<div id="parseProgress" style="position: fixed; top: 5px; right: 100px; height: 50px; width: 200px; background-color: black; color: #fff;" onclick="">... to start parsing - click here</div>';
	var siteHeader = document.getElementById("header");
	siteHeader.appendChild(divProgress);
	divProgress = document.getElementById('parseProgress');
	
	var tmp = document.createElement('div');
	tmp.innerHTML = '<div id="makeReport_btn1" style="position: relative; top: 1px; left: 1px; height: 20px; width: 80px; background-color: gray; color: #bbb;" onclick="startParser(true,1)">HTML-1</div>';
	divProgress.appendChild(tmp);
	
	var tmp = document.createElement('div');
	tmp.innerHTML = '<div id="makeReport_btn2" style="position: relative; top: 1px; left: 1px; height: 20px; width: 80px; background-color: gray; color: #bbb;" onclick="startParser(true,3)">HTML-2</div>';
	divProgress.appendChild(tmp);
	
	var tmp = document.createElement('div');
	tmp.innerHTML = '<div id="makeReport_btn3" style="position: relative; top: 1px; right: 1px; height: 20px; width: 80px; background-color: gray; color: #bbb;" onclick="startParser(true,0)">Text</div>';	
	divProgress.appendChild(tmp);
	
	productsProgressCounter	= 0;	
}, false);
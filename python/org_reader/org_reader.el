(require 'json)
(require 'org)

(defun org-replace-bib-name (LIBNAME)
  "Set 
  #+BIBLIOGRAPHY: <OLD name> <style> <options>
  to
  #+BIBLIOGRAPHY: <NEW name> <style> <options>"
  (interactive)
  (let ((case-fold-search t)
        (re (format "^#\\+%s:[ \t]+\\([^\t\n ]+\\)" "BIBLIOGRAPHY")))
    (if (not (save-excursion
               (or (re-search-forward re nil t)
                   (re-search-backward re nil t))))
        (let nil)
        ;(error (format "No line containing #+%s: value found" KEYWORD)))
    (if (string= (match-string 1) "lib")    
        (let (
              (p1 (match-beginning 1))
              (p2 (match-end 1))
              (OLDLIBNAME (match-string 1))
             )
               (delete-region p1 p2)
               (goto-char p1)
               (insert (concat LIBNAME))
        )
    )
   ))
)


(defun plist-get-as-text (plist attr)
  "Get attributes from the output of org-export-get-environment"
  (let ((str (car (plist-get plist attr))))
    (if str (substring-no-properties str) nil)))
    
    
(defun jk-get-file-keyword (KEYWORD)
  "get the value from a line like this
#+KEYWORD: value
in a file."
  (interactive)
  (let ((case-fold-search t)
        (re (format "^#\\+%s:[ \t]+\\([^\t\n]+\\)" KEYWORD)))
    (if (not (save-excursion
               (or (re-search-forward re nil t)
                   (re-search-backward re nil t))))
        (let nil)
        ;(error (format "No line containing #+%s: value found" KEYWORD)))
        (match-string 1))))
        
    
; suggested by Nicolas Goaziou
(defun jk-org-kwds ()
  "parse the buffer and return a cons list of (property . value)
from lines like:
#+PROPERTY: value"
  (org-element-map (org-element-parse-buffer 'element) 'keyword
                   (lambda (keyword) (cons (org-element-property :key keyword)
                                           (org-element-property :value keyword)))))

(defun jk-org-kwd (KEYWORD)
  "get the value of a KEYWORD in the form of #+KEYWORD: value"
  (cdr (assoc KEYWORD (jk-org-kwds))))
    
(defvar properties (org-export-get-environment))

(defun org->pelican (filename backend)
  (progn
    (save-excursion
      (find-file filename)
      (let ((propertiestwo (org-export-get-environment)))
           (org-replace-bib-name (file-name-sans-extension buffer-file-name))
        (princ (json-encode 
                (list 
                 ; Works
                 ;:title (jk-get-file-keyword "TITLE")
                 ; Works
                 :title (plist-get-as-text propertiestwo :title)
                 
                 ; Works                 
                 :date (org-timestamp-format (car (org-export-get-date propertiestwo)) "%Y-%m-%d")
                 ; Doesn't work
                 ;:keywords (plist-get propertiestwo :keywords)
                 ; Doesn't work
                 ;:keywords (plist-get-as-text propertiestwo :keywords)
				 ; Works
                 :keywords (jk-get-file-keyword "KEYWORDS")
                 
                 ; Works
                 :description (jk-get-file-keyword "DESCRIPTION")

				 ; Works
				 :tags (jk-get-file-keyword "TAGS")
                 
				 ; Works
                 ;:category (cdr (assoc "CATEGORY" org-file-properties))
				 ; Works
                 :category (jk-get-file-keyword "CATEGORY")
                 
				 ; Works
                 :lang (jk-get-file-keyword "LANG")
                 
				 ; Works
                 :translation (jk-get-file-keyword "TRANSLATION")
                 
				 ; Works
                 :summary (jk-get-file-keyword "SUMMARY")
                 
				 ; Works
                 :titless (jk-get-file-keyword "TITLESS")
                 
                 ; Works
                 :saveas (jk-get-file-keyword "SAVE_AS")
                 
				 ; Works
                 ; hidden -- hide from menu
                 :status (jk-get-file-keyword "STATUS")
                 
                 ; Works
                 ; robots -- index, follow
                 :idxstatus (jk-get-file-keyword "IDXSTATUS")
                 
				 ; Works
                 :post (org-export-as backend nil nil t)
                 )))))))

(defun org-replace-bib-name (LIBNAME)
  "Set 
  #+BIBLIOGRAPHY: <OLD name> <style> <options>
  to
  #+BIBLIOGRAPHY: <NEW name> <style> <options>"
  (interactive)
  (let ((case-fold-search t)
        (re (format "^#\\+%s:[ \t]+\\([^\t\n ]+\\)" BIBLIOGRAPHY)))
    (if (not (save-excursion
               (or (re-search-forward re nil t)
                   (re-search-backward re nil t))))
        (let nil)
        ;(error (format "No line containing #+%s: value found" KEYWORD)))
        (let (
              (p1 (match-beginning 1))
              (p2 (match-end 1))
              (url (match-string 1))
             )
             (delete-region p1 p2)
             (goto-char p1)
             (insert (concat LIBNAME ".bib"))
        )
   ))
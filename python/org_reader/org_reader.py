"""
Org Reader
==========

Based on the original version: https://github.com/getpelican/pelican-plugins/tree/master/org_reader
"""
import os
import json
import logging
import subprocess
import pprint
from pelican import readers
from pelican import signals
from pelican import settings


ELISP = os.path.join(os.path.dirname(__file__), 'org_reader.el')
LOG = logging.getLogger(__name__)

class OrgReader(readers.BaseReader):
    enabled = True

    EMACS_ARGS = ["--batch"]
    ELISP_EXEC = "(org->pelican \"{0}\" {1})"

    file_extensions = ['org']

    def __init__(self, settings):
        super(OrgReader, self).__init__(settings)
        assert 'ORG_READER_EMACS_LOCATION' in self.settings, \
            "No ORG_READER_EMACS_LOCATION specified in settings"

    def read(self, filename):
        LOG.info("Reading Org file {0}".format(filename))
        cmd = [self.settings['ORG_READER_EMACS_LOCATION']]
        cmd.extend(self.EMACS_ARGS)

        if 'ORG_READER_EMACS_SETTINGS' in self.settings:
            cmd.append('-l')
            cmd.append(self.settings['ORG_READER_EMACS_SETTINGS'])

        backend = self.settings.get('ORG_READER_BACKEND', "'html")

        # cmd.append('--debug-init')
        
        cmd.append('-l')
        cmd.append(ELISP)

        cmd.append('--eval')
        filename_esc = filename.replace('\\', '\\\\')
        cmd.append(self.ELISP_EXEC.format(filename_esc, backend))

        LOG.debug("OrgReader: running command `{0}`".format(cmd))

        json_result = subprocess.check_output(cmd)
        json_output = json.loads(json_result)

        slug, e = os.path.splitext(os.path.basename(filename))

        metadata = {'title': json_output['title'],
                    'date': json_output['date'],
                    'tags': json_output['tags'] or json_output['keywords'],
                    'summary': json_output['summary'] or json_output['description'],
                    'category': json_output['category'],
                    'slug': slug,
                    'lang': json_output['lang'] or 'ru',
                    'translation': json_output['translation'] or 'false',
                    'titless': json_output['titless'] or json_output['title'],
                    'idxstatus' : json_output['idxstatus']                    
                    }

        if 'status' in json_output:
            metadata['status'] = json_output['status']
            
        if 'save_as' in json_output:
            metadata['save_as'] = json_output['saveas']

        parsed = {}
        for key, value in metadata.items():
            parsed[key] = self.process_metadata(key, value)

        return json_output['post'], parsed

def add_reader(readers):
    readers.reader_classes['org'] = OrgReader

def register():
    signals.readers_init.connect(add_reader)

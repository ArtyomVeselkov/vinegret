(add-to-list 'load-path "~/.emacs.d/elpa/org-plus-contrib-20150914")
(add-to-list 'load-path "~/.emacs.d/elpa/htmlize-20130207.1202")
;(add-to-list 'load-path "~/.emacs.d/elpa/")
(require 'ox)
(require 'org)
(require 'ox-html)
(require 'htmlize)
(require 'ox-bibtex)